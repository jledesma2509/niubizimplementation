package com.jledesma.pasarelaworkshop.core

sealed class OperationResult{

    //Camino Satisfactorio
    data class Success(val data:String?) : OperationResult()
    //Camino Error
    data class Error(val error:String?) : OperationResult()

}
