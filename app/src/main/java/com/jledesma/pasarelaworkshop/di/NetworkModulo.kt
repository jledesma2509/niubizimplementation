package com.jledesma.pasarelaworkshop.di

import com.jledesma.pasarelaworkshop.data.Api
import com.jledesma.pasarelaworkshop.data.BasicAuthInterceptor
import com.jledesma.pasarelaworkshop.repository.PagosRepository
import com.jledesma.pasarelaworkshop.repository.PagosRepositoryImp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModulo {


    @Singleton
    @Provides
    fun provideRetrofit()  : Api.ApiInterface {

        var httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(BasicAuthInterceptor())

        val builder: Retrofit.Builder =
            Retrofit.Builder().baseUrl("https://apisandbox.vnforappstest.com/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())

        return builder.client(httpClient.build()).build().create(Api.ApiInterface::class.java)
    }

    @Singleton
    @Provides
    fun provideRepository(retrofit : Api.ApiInterface) : PagosRepository{
        return PagosRepositoryImp(retrofit)
    }

}