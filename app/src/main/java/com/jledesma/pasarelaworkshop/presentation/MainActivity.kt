package com.jledesma.pasarelaworkshop.presentation

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.jledesma.pasarelaworkshop.R
import com.jledesma.pasarelaworkshop.databinding.ActivityMainBinding
import com.jledesma.pasarelaworkshop.databinding.DialogoConfirmacionBinding
import com.jledesma.pasarelaworkshop.databinding.DialogoRechazoBinding
import com.jledesma.pasarelaworkshop.model.VisaNetRespuesta
import com.jledesma.pasarelaworkshop.model.VisaNetRespuestaError
import dagger.hilt.android.AndroidEntryPoint
import lib.visanet.com.pe.visanetlib.VisaNet
import lib.visanet.com.pe.visanetlib.data.custom.Channel
import lib.visanet.com.pe.visanetlib.presentation.custom.VisaNetViewAuthorizationCustom
import java.util.HashMap

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding

    private lateinit var visaRespuesta : VisaNetRespuesta
    private lateinit var visaNetRespuestaError: VisaNetRespuestaError

    //Suscribirlo
    private val viewModel : PagosViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        events()
        setupObservables()
    }

    private fun setupObservables() = with(binding) {

        viewModel.loader.observe(this@MainActivity, Observer {
            if(it) progressBar.visibility = View.VISIBLE
            else progressBar.visibility = View.GONE
        })

        viewModel.noAutorizado.observe(this@MainActivity, Observer {
            Toast.makeText(this@MainActivity,it,Toast.LENGTH_LONG).show()
        })

        viewModel.error.observe(this@MainActivity, Observer {
            Toast.makeText(this@MainActivity,it,Toast.LENGTH_LONG).show()
        })

        viewModel.token.observe(this@MainActivity, Observer { token ->
            token?.let {
                payNibuz(it)
            }
        })
    }

    private fun payNibuz(token:String){

        val comercio = "456879852"
        val orden = "00001" //max 12 digitos
        val total = binding.tvMonto.text.toString().toDouble()

        //MetaData
        val data: MutableMap<String, Any> = HashMap()

        data[VisaNet.VISANET_SECURITY_TOKEN] = token
        data[VisaNet.VISANET_CHANNEL] = Channel.MOBILE
        data[VisaNet.VISANET_COUNTABLE] = true
        data[VisaNet.VISANET_MERCHANT] = comercio
        data[VisaNet.VISANET_PURCHASE_NUMBER] = orden
        data[VisaNet.VISANET_AMOUNT] = total

        val MDDdata = HashMap<String, String>()
        MDDdata["4"] = "jledesma2509@gmail.com"
        MDDdata["21"] = "1"
        MDDdata["32"] = "001"
        MDDdata["75"] = "Registrado"
        MDDdata["77"] = "0"

        data[VisaNet.VISANET_MDD] = MDDdata

        data[VisaNet.VISANET_ENDPOINT_URL] = "https://apisandbox.vnforappstest.com/"

        data[VisaNet.VISANET_CERTIFICATE_HOST] = "apisandbox.vnforappstest.com"

        //Obtener desde otro servicio
        data[VisaNet.VISANET_CERTIFICATE_PIN] = "sha256/HeaAN+lVVXnHuaGtVpaN5mtnJcPi6BfdcEY556M0RA0="

        val custom = VisaNetViewAuthorizationCustom()
        custom.buttonColorMerchant = R.color.colorAccent

        try{
            VisaNet.authorization(this,data,custom)
        }catch (ex:Exception){
            Toast.makeText(this,ex.message,Toast.LENGTH_LONG).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        try{
            if (requestCode == VisaNet.VISANET_AUTHORIZATION) {
                if (data != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        val JSONString = data.extras?.getString("keySuccess")

                        val gson = Gson()
                        visaRespuesta = gson.fromJson(JSONString, VisaNetRespuesta::class.java)

                        visaRespuesta?.let {
                            createDialogActionSatisfactorio(it).show()
                        }


                    } else {
                        var JSONString = data.extras!!.getString("keyError")
                        JSONString = JSONString ?: ""

                        val gson = Gson()
                        visaNetRespuestaError = gson.fromJson(JSONString, VisaNetRespuestaError::class.java)

                        visaNetRespuestaError?.let {
                            createDialogActionRechazo(it).show()
                        }
                    }
                } else {
                    Toast.makeText(this,"Hubo un problema al procesar su solicitud. Intentelo mas tarde",Toast.LENGTH_LONG).show()
                }
            }
        }catch (ex:Exception){
            Toast.makeText(this,"Hubo un problema al procesar su solicitud. Intentelo mas tarde",Toast.LENGTH_LONG).show()
        }
    }

    private fun events() = with(binding){

        btnComprar.setOnClickListener {

            viewModel.getTokenNiubiz()

        }

    }

    fun createDialogActionSatisfactorio(visaRespuesta: VisaNetRespuesta): AlertDialog {

        val bindingAlert = DialogoConfirmacionBinding.inflate(LayoutInflater.from(this))
        val builder = AlertDialog.Builder(this)

        builder.setView(bindingAlert.root)

        val alertDialog = builder.create()
        alertDialog.setCancelable(false)

        bindingAlert.tvNroPedidoNiubuz.text = visaRespuesta.order.purchaseNumber
        bindingAlert.tvClienteNiubuz.text = "Nombres"
        bindingAlert.tvTarjetaNiubuz.text = "${visaRespuesta.dataMap.BRAND} ${visaRespuesta.dataMap.CARD}"
        bindingAlert.tvImporteNiubiz.text = visaRespuesta.dataMap.AMOUNT

        val fechaNiubiz = visaRespuesta.dataMap.TRANSACTION_DATE  //"201010173430"
        val anio = fechaNiubiz.substring(0,2)
        val mes = fechaNiubiz.substring(2,4)
        val dia = fechaNiubiz.substring(4,6)
        val hora = fechaNiubiz.substring(6,8)
        val minuto = fechaNiubiz.substring(8,10)
        val segundo = fechaNiubiz.substring(10,12)

        bindingAlert.tvFechaNiubiz.text = "$dia/$mes/$anio $hora:$minuto:$segundo"

        bindingAlert.btnOkDialogo.setOnClickListener {
            alertDialog.dismiss()
        }

        return alertDialog
    }

    fun createDialogActionRechazo(visaRespuestaDenegada: VisaNetRespuestaError): AlertDialog {

        val bindingAlert = DialogoRechazoBinding.inflate(LayoutInflater.from(this))
        val builder = AlertDialog.Builder(this)

        builder.setView(bindingAlert.root)

        val alertDialog = builder.create()
        alertDialog.setCancelable(false)

        bindingAlert.tvNroPedidoNiubuz.text = "-"
        bindingAlert.tvClienteNiubuz.text = "Nombres"
        bindingAlert.tvTarjetaNiubuz.text = "${visaRespuestaDenegada.data.BRAND} ${visaRespuestaDenegada.data.CARD}"
        bindingAlert.tvImporteNiubiz.text = visaRespuestaDenegada.data.AMOUNT
        bindingAlert.tvMotivoRechazoNiubiz.text = visaRespuestaDenegada.data.ACTION_DESCRIPTION

        val fechaNiubiz = visaRespuestaDenegada.data.TRANSACTION_DATE  //"201010173430"
        val anio = fechaNiubiz.substring(0,2)
        val mes = fechaNiubiz.substring(2,4)
        val dia = fechaNiubiz.substring(4,6)
        val hora = fechaNiubiz.substring(6,8)
        val minuto = fechaNiubiz.substring(8,10)
        val segundo = fechaNiubiz.substring(10,12)

        bindingAlert.tvFechaNiubiz.text = "$dia/$mes/$anio $hora:$minuto:$segundo"

        bindingAlert.btnOkDialogo.setOnClickListener {
            alertDialog.dismiss()
        }

        return alertDialog
    }
}