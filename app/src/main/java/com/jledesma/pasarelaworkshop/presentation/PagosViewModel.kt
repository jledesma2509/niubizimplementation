package com.jledesma.pasarelaworkshop.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jledesma.pasarelaworkshop.core.OperationResult
import com.jledesma.pasarelaworkshop.data.Api
import com.jledesma.pasarelaworkshop.repository.PagosRepository
import com.jledesma.pasarelaworkshop.repository.PagosRepositoryImp
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class PagosViewModel @Inject constructor(val pagosRepository: PagosRepository)
    : ViewModel() {

    //MutableLiveData y LiveData
    private val _loader = MutableLiveData<Boolean>()
    val loader : LiveData<Boolean> = _loader

    private val _error = MutableLiveData<String>()
    val error : LiveData<String> = _error

    private val _token = MutableLiveData<String>()
    val token : LiveData<String> = _token

    private val _noAutorizado = MutableLiveData<String>()
    val noAutorizado : LiveData<String> = _noAutorizado



    fun getTokenNiubiz() {

        viewModelScope.launch() {

            _loader.value = true

            try{
                val response = withContext(Dispatchers.IO){
                    pagosRepository.getToken()
                }

                when(response){
                    is OperationResult.Error -> {
                        _error.value = response.error
                    }
                    is OperationResult.Success -> {
                        _token.value = response.data.toString()
                    }
                }

                /*if(response.isSuccessful){
                    _token.value = response.body()
                }
                else{
                    if(response.code() == 401) _noAutorizado.value = "No estas autorizado. Intentalo nuevamente"
                    else _error.value = response.message()
                }*/

            }catch (ex:Exception){
                _error.value = ex.toString()
            }finally {
                _loader.value = false
            }

        }
    }
}