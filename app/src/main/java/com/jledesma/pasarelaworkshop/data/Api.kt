package com.jledesma.pasarelaworkshop.data

import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.POST

object Api {

    private val builder: Retrofit.Builder =
        Retrofit.Builder().baseUrl("https://apisandbox.vnforappstest.com/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())

    interface ApiInterface{

        @POST("api.security/v1/security")
        suspend fun authNiubiz(): Response<String>
    }

    //fun build() : ApiInterface{
    //    return builder.build().create(ApiInterface::class.java)
    //}

    //Autenticacion basica
    fun build() : ApiInterface{

        //Agregando para la autenticacion Basica
        var httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(BasicAuthInterceptor())

        return builder.client(httpClient.build()).build().create(ApiInterface::class.java)
    }
}