package com.jledesma.pasarelaworkshop.data

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response

class BasicAuthInterceptor() : Interceptor {

    private var basicCredentials : String = Credentials.basic("integraciones@niubiz.com.pe","_7z3@8fF")
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder().header("Authorization", basicCredentials).build()
        return chain.proceed(request)
    }
}