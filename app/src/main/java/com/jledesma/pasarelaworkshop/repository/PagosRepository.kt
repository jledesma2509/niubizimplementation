package com.jledesma.pasarelaworkshop.repository

import com.jledesma.pasarelaworkshop.core.OperationResult

interface PagosRepository {

    suspend fun getToken() : OperationResult
}