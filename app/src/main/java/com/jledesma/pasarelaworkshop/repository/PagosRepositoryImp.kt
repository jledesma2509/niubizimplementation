package com.jledesma.pasarelaworkshop.repository

import com.jledesma.pasarelaworkshop.core.OperationResult
import com.jledesma.pasarelaworkshop.data.Api

class PagosRepositoryImp constructor(val retrofit : Api.ApiInterface)  : PagosRepository {


    override suspend fun getToken(): OperationResult {

        return try{
            val response = retrofit.authNiubiz()

            if(response.isSuccessful){
                val token = response.body()
                OperationResult.Success(token)
            }
            else{
                if(response.code() == 401){
                    OperationResult.Error("No estas autorizado. Intentalo nuevamente")
                }
                else  OperationResult.Error(response.message())
            }

        }catch (ex:Exception){
            OperationResult.Error(ex.message)
        }
    }
}