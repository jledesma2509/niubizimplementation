package com.jledesma.pasarelaworkshop.model

data class VisaNetRespuesta(
    val dataMap : DataMap,
    val header: Header,
    val fulfillment : FulFillment,
    val order : Order
)

data class VisaNetRespuestaError(
    val errorCode: Int,
    val errorMessage: String,
    val header: Header,
    val data: DataError
)

data class DataError(
    val CURRENCY: String,
    val TRANSACTION_DATE: String,
    val TERMINAL: String,
    val ACTION_CODE: String,
    val TRACE_NUMBER: String,
    val ECI_DESCRIPTION: String,
    val CARD_TYPE: String,
    val ECI: String,
    val SIGNATURE: String,
    val CARD: String,
    val BRAND: String,
    val MERCHANT: String,
    val STATUS: String,
    val ADQUIRENTE: String,
    val ACTION_DESCRIPTION: String,
    val ID_UNICO: String,
    val AMOUNT: String,
    val PROCESS_CODE: String,
    val TRANSACTION_ID: String
)

data class DataMap(
    val ACTION_CODE: String,
    val ACTION_DESCRIPTION: String,
    val ADQUIRENTE: String,
    val AMOUNT: String,
    val AUTHORIZATION_CODE: String,
    val BRAND: String,
    val CARD: String,
    val ECI: String,
    val ECI_DESCRIPTION: String,
    val ID_UNICO: String,
    val MERCHANT: String,
    val PROCESS_CODE: String,
    val RECURRENCE_MAX_AMOUNT: String,
    val RECURRENCE_STATUS: String,
    val SIGNATURE: String,
    val STATUS: String,
    val TERMINAL: String,
    val TRACE_NUMBER: String,
    val TRANSACTION_DATE: String,
    val TRANSACTION_ID: String
)

data class Header(
    val ecoreTransactionDate: Double,
    val ecoreTransactionUUID: String,
    val millis: Double
)

data class FulFillment(
    val captureType: String,
    val channel: String,
    val countable: Boolean,
    val fastPayment: Boolean,
    val merchantId: String,
    val signature: String,
    val terminalId: String
)

data class Order(
    val actionCode: String,
    val amount: Double,
    val authorizationCode: String,
    val authorizedAmount: Double,
    val currency: String,
    val productId: String,
    val purchaseNumber: String,
    val traceNumber: String,
    val transactionDate: String,
    val transactionId: String
)
